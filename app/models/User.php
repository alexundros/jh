<?php

namespace jh\frontend\Models;

class User extends \Phalcon\Mvc\Model
{
public $id;
public $name;
public $login;
public $password;

    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("user");
    }

    public function getSource()
    {
        return 'user';
    }
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
    public function checkPass( $pass )
    {
        return $this->getDi()->getShared( 'security' )
            ->checkHash( $pass, $this->pass );
    }
}