<?php

class Vacancies extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idVacancies;

    /**
     *
     * @var integer
     */
    public $premium;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var integer
     */
    public $idDepartment;

    /**
     *
     * @var integer
     */
    public $hasTest;

    /**
     *
     * @var integer
     */
    public $responseLetterRequired;

    /**
     *
     * @var integer
     */
    public $idArea;

    /**
     *
     * @var integer
     */
    public $idSalary;

    /**
     *
     * @var integer
     */
    public $idType;

    /**
     *
     * @var integer
     */
    public $idAddress;

    /**
     *
     * @var string
     */
    public $responseUrl;

    /**
     *
     * @var integer
     */
    public $sortPointDistanse;

    /**
     *
     * @var integer
     */
    public $idEmployer;

    /**
     *
     * @var string
     */
    public $publishedAt;

    /**
     *
     * @var string
     */
    public $created_At;

    /**
     *
     * @var integer
     */
    public $archived;

    /**
     *
     * @var string
     */
    public $applyAlternateUrl;

    /**
     *
     * @var integer
     */
    public $idInsiderInterview;

    /**
     *
     * @var string
     */
    public $url;

    /**
     *
     * @var string
     */
    public $alternateUrl;

    /**
     *
     * @var integer
     */
    public $idSnippet;

    /**
     *
     * @var integer
     */
    public $idContacts;

    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("vacancies");
        $this->belongsTo('idAddress', 'Address', 'idAddress', ['alias' => 'Address']);
        $this->belongsTo('idArea', 'Area', 'idArea', ['alias' => 'Area']);
        $this->belongsTo('idContacts', 'Contacts', 'idContacts', ['alias' => 'Contacts']);
        $this->belongsTo('idDepartment', 'Department', 'idDepartment', ['alias' => 'Department']);
        $this->belongsTo('idEmployer', 'Employer', 'idEmployer', ['alias' => 'Employer']);
        $this->belongsTo('idInsiderInterview', 'InsiderInterview', 'idInsiderInterview', ['alias' => 'InsiderInterview']);
        $this->belongsTo('idSalary', 'Salary', 'idSalary', ['alias' => 'Salary']);
        $this->belongsTo('idSnippet', 'Snippet', 'idSnippet', ['alias' => 'Snippet']);
        $this->belongsTo('idType', 'Type', 'idType', ['alias' => 'Type']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'vacancies';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Vacancies[]|Vacancies|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Vacancies|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
