<?php

class Contacts extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idContacts;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var integer
     */
    public $idPhones;

    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("contacts");
        $this->hasMany('idContacts', 'Vacancies', 'idContacts', ['alias' => 'Vacancies']);
        $this->belongsTo('idPhones', 'Phones', 'idPhones', ['alias' => 'Phones']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'contacts';
    }
}
