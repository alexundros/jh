<?php

class Snippet extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idSnippet;

    /**
     *
     * @var string
     */
    public $requirment;

    /**
     *
     * @var string
     */
    public $responsibility;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("snippet");
        $this->hasMany('idSnippet', 'Vacancies', 'idSnippet', ['alias' => 'Vacancies']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'snippet';
    }
}
