<?php

class Phones extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idPhones;

    /**
     *
     * @var string
     */
    public $comment;

    /**
     *
     * @var integer
     */
    public $city;

    /**
     *
     * @var integer
     */
    public $number;

    /**
     *
     * @var integer
     */
    public $country;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("phones");
        $this->hasMany('idPhones', 'Contacts', 'idPhones', ['alias' => 'Contacts']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'phones';
    }
}
