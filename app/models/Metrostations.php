<?php

class Metrostations extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idMetroStations;

    /**
     *
     * @var string
     */
    public $stations_Name;

    /**
     *
     * @var string
     */
    public $line_Name;

    /**
     *
     * @var string
     */
    public $station_Id;

    /**
     *
     * @var integer
     */
    public $line_Id;

    /**
     *
     * @var string
     */
    public $lat;

    /**
     *
     * @var string
     */
    public $lng;

    /**
     *
     * @var integer
     */
    public $idAddress;

    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("metrostations");
        $this->belongsTo('idAddress', 'Address', 'idAddress', ['alias' => 'Address']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'metrostations';
    }
}
