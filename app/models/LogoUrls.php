<?php

class LogoUrls extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idLogoUrls;

    /**
     *
     * @var string
     */
    public $240;

    /**
     *
     * @var string
     */
    public $90;

    /**
     *
     * @var string
     */
    public $original;

    public function initialize()
    {
        $this->setSchema("hh");
        $this->setSource("logo_urls");
        $this->hasMany('idLogoUrls', 'Employer', 'idLogoUrls', ['alias' => 'Employer']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'logo_urls';
    }
}
