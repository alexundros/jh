<?php

class Employer extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idEmployer;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $url;

    /**
     *
     * @var string
     */
    public $alternateUrl;

    /**
     *
     * @var integer
     */
    public $idLogoUrls;

    /**
     *
     * @var string
     */
    public $vacanciesUrl;

    /**
     *
     * @var integer
     */
    public $trusted;

    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("employer");
        $this->hasMany('idEmployer', 'Vacancies', 'idEmployer', ['alias' => 'Vacancies']);
        $this->belongsTo('idLogoUrls', 'LogoUrls', 'idLogoUrls', ['alias' => 'LogoUrls']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'employer';
    }
}
