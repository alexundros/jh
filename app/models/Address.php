<?php

class Address extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idAddress;

    /**
     *
     * @var string
     */
    public $city;

    /**
     *
     * @var string
     */
    public $street;

    /**
     *
     * @var string
     */
    public $building;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $lat;

    /**
     *
     * @var string
     */
    public $lng;

    /**
     *
     * @var string
     */
    public $raw;

    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("address");
        $this->hasMany('idAddress', 'Metro', 'idAddress', ['alias' => 'Metro']);
        $this->hasMany('idAddress', 'Metrostations', 'idAddress', ['alias' => 'Metrostations']);
        $this->hasMany('idAddress', 'Vacancies', 'idAddress', ['alias' => 'Vacancies']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'address';
    }
}
