<?php

class InsiderInterview extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idInsiderInterview;

    /**
     *
     * @var string
     */
    public $url;

    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("insider_interview");
        $this->hasMany('idInsiderInterview', 'Vacancies', 'idInsiderInterview', ['alias' => 'Vacancies']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'insider_interview';
    }
}
