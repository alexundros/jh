<?php

class Type extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idType;

    /**
     *
     * @var string
     */
    public $idTypes;

    /**
     *
     * @var string
     */
    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("type");
        $this->hasMany('idType', 'Vacancies', 'idType', ['alias' => 'Vacancies']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'type';
    }
}
