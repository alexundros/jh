<?php

class Area extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idArea;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $url;

    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("area");
        $this->hasMany('idArea', 'Vacancies', 'idArea', ['alias' => 'Vacancies']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'area';
    }
}
