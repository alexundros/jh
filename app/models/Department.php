<?php

class Department extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idDepartment;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $iddDepartment;

    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("department");
        $this->hasMany('idDepartment', 'Vacancies', 'idDepartment', ['alias' => 'Vacancies']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'department';
    }
}
