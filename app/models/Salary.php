<?php

class Salary extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idSalary;

    /**
     *
     * @var integer
     */
    public $from;

    /**
     *
     * @var integer
     */
    public $to;

    /**
     *
     * @var string
     */
    public $currency;

    /**
     *
     * @var integer
     */
    public $gross;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        //$this->setSchema("hh");
        //$this->setSource("salary");
        $this->hasMany('idSalary', 'Vacancies', 'idSalary', ['alias' => 'Vacancies']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'salary';
    }
}
