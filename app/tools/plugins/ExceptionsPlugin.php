<?php
namespace jh\frontend\tools\plugins;
use Exception;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Dispatcher\Exception as DispatchException;

class ExceptionsPlugin
{
    public function beforeException(Event $event,
                                    Dispatcher $dispatcher,
                                    Exception $exception)
    {
        $code = $exception->getCode();
        $message = $exception->getMessage();
        switch ($code) {
            case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
            case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                {
                    $code = 404;
                    $message = "Страницы не существует!";
                }
                break;
        }

        $dispatcher->forward(
            [
                'controller' => 'error',
                'action' => 'index',
                'params' => ["code" => $code, "message" => $message]
            ]
        );

        return false;
    }
}