<?php

namespace jh\frontend\tools\validation;

use \Phalcon\Validation;
use \Phalcon\Validation\Validator\PresenceOf;

class AuthorizateSessionValidation extends Validation {
	 public function initialize()
    {
    	//проверяем, был ли передан логин
    	$this->add(
    		'login',
    		new PresenceOf(
    			[
    				'message' => 'Введите логин!!!',
    			]
    		)
    	);
    	//проверяем, был ли передан пароль
    	$this->add(    		
    		'pass',
    		new PresenceOf(
    			[
    				'message' => 'Введите пароль!!!',
    			]
    		)
    	);
    }
}