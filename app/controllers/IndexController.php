<?php

use Phalcon\Mvc\View;

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->flashSession->output();
    }

    public function indexAction()
    {

        $this->flashSession->output();

//        $this->view->disableLevel(
//            View::LEVEL_MAIN_LAYOUT
//        );

        $this->view->messages = $this->dispatcher->getParam('error');
        /*  $this->view->messages = $this->security->hash("testpass");
         *  testpass:
         *  $2y$12$eVNhL0dZT1QzVEVmbHYrUu9qxHAKqINegWqEIaHAmTOCqbqqDWeOG
         */
    }
    public function adminAuthorizationAction()
    {
//        $this->view->disableLevel(
//            View::LEVEL_MAIN_LAYOUT
//        );

        $this->view->messages = $this->dispatcher->getParam('error');
    }
}

