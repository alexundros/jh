<?php

use \Phalcon\Mvc\Controller;
use \Phalcon\Validation\Validator\PresenceOf;
use jh\frontend\Models\User;
use jh\frontend\tools\validation\AuthorizateSessionValidation as Validation;

class SessionController extends Controller
{
    private function _registerSession($user, $role)
    {
        $this->session->set(
            'auth',
            [
                'role' => $role,
                'id' => $user->id,
                'login' => $user->login,
            ]
        );
    }
    public function adminAuthorizateAction()
    {
        $error="";
        if ($this->request->isPost()) {
            $validation = new Validation();
            $messages = $validation->validate($_POST);

            if (!(count($messages)>0)) {
                $login = $this->request->getPost('login');
                $pass = $this->request->getPost('pass');

                $user = User::findFirst(
                    [
                        'login=:login:',
                        'bind' => [
                            'login' => $login,
                        ]
                    ]
                );

                if ($user!== false && $user->checkPass($pass)) {
                    $this->_registerSession($user, 'Admin');
                    $this->flashSession->success(
                        'Здравствуйте, '.$user->login . '!'
                    );

                    return $this->response->redirect('index');
                }
                $this->flash->error(
                    "Неверный логин или пароль!!!"
                );
                $error="Неверный логин или пароль!!! ";
            } else {
                foreach ($messages as $message) {
                    $error=$message;
                }
                $this->flash->error(
                    $error
                );
            }
        }

        return $this->dispatcher->forward(
            [
                'controller' =>
                    'index',
                'action' => 'adminAuthorization'
            ]
        );
    }
    public function logoutAction()
    {
        if (!empty($this->session->get('auth'))) {
            $this->session->destroy();
        }
        if ($this->session->get('auth')['role']=='Admin') {
            $this->response->redirect('admin');
        } else {
            $this->response->redirect('');
        }
    }
}