<?php

$loader = new \Phalcon\Loader();
$loader->registerNamespaces(
    [
        "jh\\frontend\\Controllers" => APP_PATH."/controllers",
        "jh\\frontend\\Models"      => APP_PATH."/models",
        "jh\\frontend\\tools\\validation"   => APP_PATH.'/tools/validation',
        "jh\\frontend\\tools\\plugins"      => APP_PATH.'/tools/plugins',
    ]
);

$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir
    ]
);
$loader->register();
