<?php

$router = $di->getRouter();

// Define your routes here
// правило для входа в админку
$router->add(
    "/admin/:params",
    [
        'controller' => 'index',
        'action' => 'adminAuthorization',
        'params' => 1,
    ]
);

$router->add(
    "/:controller/:action/([0-9]+)/([0-9]+)/:params",
    [
        'controller' => 1,
        'action' => 2,
        'id' => 3,
        'page' => 4,
        'params' => 5,
    ]
);

$router->add(
    "/:controller/([0-9]+)/:params",
    [
        'controller' => 1,
        'action' => 'index',
        'id' => 2,
        'page' => 3,
        'params' => 4,
    ]
);

$router->handle();
