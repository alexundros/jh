<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Events\Manager as EventsManager;
use jh\frontend\tools\plugins\ExceptionsPlugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Session\Adapter\Files as Session;
use Phalcon\Flash\Direct as FlashDirect;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Mvc\Applicat;
use Phalcon\Mvc\Model\Manager as ModelsManager;

$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    return $view;
});

$di->setShared('db', function () {
    $config = $this->getConfig();
    $db = $config->database;

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $db->adapter;
    $params = [
        'host'     => $db->host,
        'username' => $db->username,
        'password' => $db->password,
        'dbname'   => $db->dbname,
        'charset'  => $db->charset
    ];

    if ($db->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});

$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

$di->set(
    "session",
    function () {
        $session = new Session();
        $session->start();

        return $session;
    }
);

$di->set(
    'flash',
    function () {
        return new FlashDirect([
            'error'   => 'alert alert-danger mt-1',
            'success' => 'alert alert-success mt-1',
            'notice'  => 'alert alert-info mt-1',
            'warning' => 'alert alert-warning mt-1',
        ]);
    }
);

$di->set(
    'flashSession',
    function () {
        return new FlashSession([
            'error'   => 'alert alert-danger my-1',
            'success' => 'alert alert-success my-1',
            'notice'  => 'alert alert-info my-1',
            'warning' => 'alert alert-warning my-1',
        ]);
    }
);

$di->set(
    "modelsManager",
    function() {
        return new ModelsManager();
    }
);

$di->set(
    "dispatcher",
    function () {
        // Создаем менеджер событий
        $eManager = new EventsManager();

        $eManager->attach(
            "dispatch:beforeException",
            new ExceptionsPlugin()
        );

        $dispatcher = new Dispatcher();
        $dispatcher->setEventsManager($eManager);

        return $dispatcher;
    }
);